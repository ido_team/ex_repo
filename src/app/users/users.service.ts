import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';
import {AngularFire} from 'angularfire2'; 
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class UsersService {
  private _url = "http://jsonplaceholder.typicode.com/users";
  usersObservable;

  constructor(private af:AngularFire) { }
  
  addUser(user){
    this.usersObservable.push(user);
  }

  deleteUser(user){
    this.af.database.object('/users/' + user.$key).remove();
    console.log('/users/' + user.$key);
  }

  updateUser(user){
    let user1 = {name:user.name,email:user.email};
    console.log(user1);
    this.af.database.object('/users/' + user.$key).update(user1);
  }  

  getUsers(){
    this.usersObservable = this.af.database.list('/users').map(
      users =>{
        users.map(
          user=>{
            user.postTitles = [];
            for(var p in user.posts){
              user.postTitles.push(
                this.af.database.object('/posts'  + p)
              );
            }
          }
        );
        return users;
      }
    )
    return this.usersObservable;
	}


}