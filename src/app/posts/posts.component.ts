import { Component, OnInit } from '@angular/core';
import {PostsService} from './posts.service';


@Component({
  selector: 'jce-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})

export class PostsComponent implements OnInit {

posts;
isLoading:boolean = true;

showPost(){
  console.log(this)
}
  constructor(private _postsService: PostsService) { }
  ngOnInit() {
     this._postsService.getPosts()
			    .subscribe(posts => {this.posts = posts;
                               this.isLoading = false;
                               console.log(posts)});
  }
  
  deletePost(post){
    this._postsService.deletePost(post);
  }
  addPost(post){
    this._postsService.addPost(post);
  }
  editPost(post){
    this._postsService.updatePost(post);
  }


}
