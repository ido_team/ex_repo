import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Post } from '../post/post';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'jce-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {

post:Post = {title:'', body:'', id:''};
@Output() postAddedEvent = new EventEmitter<Post>();

onSubmit(form:NgForm){
  //console.log(form.form.value);
  this.postAddedEvent.emit(this.post);
  this.post = {title: '', body: '',id:''};
}
  constructor() { }

  ngOnInit() {
  }

}
