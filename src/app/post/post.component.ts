import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Post } from '../post/post';

@Component({
  selector: 'jce-post',
  templateUrl: './post.component.html',
  styles:[`
    .posts{border:1px solid; margin-top:5px;}
  `],
  inputs:['post', 'undo']
})
export class PostComponent implements OnInit {


undo:Boolean = false;
post:Post;
@Output() deleteEvent = new EventEmitter();
@Output() editEvent = new EventEmitter();

isEdit:Boolean = false;
editButtonText = "Edit";
postTitle:String;
postBody:String;

  constructor() {
   }

  sendDelete(){
      this.deleteEvent.emit(this.post);
    }

   toggleEdit(){
    console.log(this.undo);
    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText ="Save" : this.editButtonText = "Edit";

    if(!this.isEdit){
      this.editEvent.emit(this.post);
    }
  }


  ngOnInit() { 
    this.postTitle = this.post.title;
    this.postBody = this.post.body;
  }  



  cancel(){
    if(!this.undo){
      this.post.title = this.postTitle;
      this.post.body = this.postBody;
    }
  this.isEdit = !this.isEdit;
  this.isEdit ? this.editButtonText ="Save" : this.editButtonText = "Edit";

 }

}
